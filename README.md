# Use SRTM in WPS

## Download the data
Data can be downloaded from [1].

## Run the python script
```
python make_wps_static_tiles.py
```

## Prepare the static data folder
Go in your WPS_GEOG folder.
Create a new folder "topo_srtm_1s"
Copy the tiles in this folder.
Paste the index file in this folder. 


[1]: https://dwtkns.com/srtm30m/
